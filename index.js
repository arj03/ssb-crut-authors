const Crut = require('ssb-crut')
const ComplexSet = require('@tangle/complex-set')
const Defns = require('ssb-schema-definitions')
const clone = require('lodash.clonedeep')

const ProcessAuthors = require('./process-authors')

const authors = ComplexSet(`^(\\*|${Defns().feedId.pattern})$`)

module.exports = class CrutAuthors extends Crut {
  constructor (server, spec) {
    if ('authors' in spec.props) throw new Error('ssb-crut-authors does not allow spec.props.authors')
    if (spec.staticProps && 'authors' in spec.staticProps) {
      throw new Error('ssb-crut-authors does not allow spec.staticProps.authors')
    }

    spec = clone(spec)
    /* extend the spec */
    spec.props.authors = authors
    const isValidNextStep = IsValidNextStep(spec.tangle || spec.type.split('/')[0])

    if (!spec.isValidNextStep) spec.isValidNextStep = isValidNextStep
    else {
      const _super = spec.isValidNextStep
      spec.isValidNextStep = function isValidNextStep (context, msg) {
        isValidNextStep(context, msg) && _super(context, msg)
      }
    }

    /* classic crut */
    super(server, spec)

    /* common helpers */
    this.processAuthors = ProcessAuthors(server)
  }

  create (input, cb) {
    input = clone(input) // processAuthors mutates input
    this.processAuthors(input, (err, input) => {
      if (err) return cb(err)

      super.create(input, cb)
    })
  }

  // read  << inherit

  update (id, input, cb) {
    input = clone(input) // processAuthors mutates input
    this.processAuthors(input, (err, input) => {
      if (err) return cb(err)

      super.update(id, input, cb)
    })
  }
}

/* isValidNextStep */

function IsValidNextStep (tangle) {
  const _isRoot = m => (m.value.content.tangles[tangle].previous === null)

  return function isValidNextStep (context, msg) { // TODO: move this out
    isValidNextStep.error = null

    // if it's an update
    if (!_isRoot(msg)) {
      // check if the author of this message is in the authors
      const validAuthor = isValidAuthor(authors.mapToOutput(context.accT.authors), msg.value.author, msg.value.sequence)
      if (!validAuthor) {
        isValidNextStep.error = new Error(
          `Invalid author: ${msg.value.author} not in ${JSON.stringify(authors.mapToOutput(context.accT.authors))}`
        )
        return false
      }
    }

    if (!isValidAuthorChange(context, msg)) {
      isValidNextStep.error = _isRoot(msg)
        ? new Error('Invalid initial authors')
        : new Error('Invalid authors change')
      return false
    }

    return true
  }
}
function isValidAuthor (authorsState, author, seq) {
  // authorsState : reified state of authors field
  // author : FeedId | *

  if (authorsState['*']) {
    if (isActiveAuthor(authorsState['*'], Date.now())) return true
  }

  if (authorsState[author]) {
    return isActiveAuthor(authorsState[author], seq)
  }
  return false
}
function isActiveAuthor (intervals, seq) {
  return intervals.some(({ start, end }) => {
    return (
      (seq >= start) &&
      (end === null || seq <= end)
    )
  })
}
function isValidAuthorChange (context, msg) {
  const A = context.accT.authors
  const B = msg.value.content.authors

  // see what the context of authors would be if this transformation
  // was applied
  const c = authors.mapToOutput(authors.concat(A, B))

  // there are no authors if all intervals for all authors are "closed"
  return Object.values(c).some(intervals => {
    return intervals.some(({ end }) => end === null)
  })
}
