const pull = require('pull-stream')
const { isFeedId } = require('ssb-ref')

// this takes input.authors.add: [ Author ]
// and mutates them to input.authors.add [{ id: Author, seq }]
//   - if Author is a FeedId, seq is the current know sequence of that author
//   - if Author is '*', seq is Date.now() (current Unix Time)

module.exports = function ProcessAuthors (server) {
  return function processAuthors (update, cb) {
    if (!update.authors) return cb(null, update)

    if (update.authors.add && !Array.isArray(update.authors.add)) {
      return cb(new Error('cannot create profile, malformed authors.add ' + update.authors.add))
    }

    if (update.authors.remove && !Array.isArray(update.authors.remove)) {
      return cb(new Error('cannot create profile, malformed authors.remove ' + update.authors.remove))
    }

    // assign sequences to each feedId on both the add + removes
    getAuthorsSequences(update.authors.add, (err, add) => {
      if (err) return cb(err)
      if (add) update.authors.add = add

      getAuthorsSequences(update.authors.remove, (err, remove) => {
        if (err) return cb(err)
        if (remove) update.authors.remove = remove

        cb(null, update)
      })
    })
  }

  function getAuthorsSequences (authorsArry, cb) {
    if (authorsArry === undefined) return cb(null, null)
    if (!authorsArry || authorsArry.length === 0) return cb(null, [])

    pull(
      pull.values(authorsArry),
      pull.asyncMap((authorFeedId, cb) => {
        if (authorFeedId === '*') return cb(null, { id: '*', seq: Number(Date.now()) })
        if (!isFeedId(authorFeedId)) return cb(new Error(`malformed feedId on authors ${authorFeedId}`))

        server.getFeedState(authorFeedId, (err, { sequence }) => {
          if (err) return cb(err)

          cb(null, { id: authorFeedId, seq: sequence || 0 })
        })
      }),
      pull.collect((err, result) => {
        if (err) return cb(err)

        cb(null, result)
      })
    )
  }
}
