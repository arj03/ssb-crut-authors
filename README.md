# ssb-crut-authors

Basically `ssb-crut` but adds `authors` field and rules which go with that
to control who can publish valid updates to a record.

## Example usage

```js
const CRUT = require('ssb-crut-authors')
const Overwrite = require('@tangle/overwrite')

const server = Server() // some scuttlebutt instance
const spec = {
  type: 'gathering',
  props: {
    title: Overwrite({ type: 'string' }),
    description: Overwrite({ type: 'string' }),
    date: Overwrite({ type: 'string', pattern: '^2020-\d\d-\d\d' }),
    // authors    <-- automatically added
  },
  // isValidNextStep    <-- automatically added to read authors, you can add another layer on top though
}

const crut = new CRUT(serve, spec)

const details = {
  title: 'party',
  authors: { add: [server.id] }
}
crut.create(details, (err, gatheringId) => {
  //
})
```

## Authorship Rules

1. You must add at least one author when creating a record
2. Any update which would leave a record with no authors is invalid (and blocked / ignored)
3. An `Author` must be:
  - a `FeedId`
  - `*`, the wildcard, meaning anyone can author
4. If `*` is active (has been added and not removed) then anyone can update, regardless of whehter they have been removed.

## API

Same as `ssb-crut` except `spec.props` and `spec.staticProps` cannot include `authors`

## TODO

This was extracted from `ssb-profile` in order to generalise it to more records.
  - Need to review if we've extracted all the relevant tests about authorship from there

We might want to consider modifying `ssb-crut` to take "plugins" to extend core functionality and make this a plugin
