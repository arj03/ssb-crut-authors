const test = require('tape')
const Crut = require('../')
const { Server, Spec } = require('./helpers')

test('tombstone', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const init = { body: 'dinosaur notes', authors: { add: [alice.id] } }
  alice.wiki.create(init, (err, id) => {
    t.error(err, 'alice creates a record with self as author')

    const A = { reason: 'epistomology notes' }
    alice.wiki.tombstone(id, A, (err, updateA) => {
      t.error(err, 'alice can tombstone')

      alice.close()
      t.end()
    })
  })
})
