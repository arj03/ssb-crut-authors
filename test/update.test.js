const test = require('tape')
const Crut = require('../')
const { Server, Spec, replicate, compareRead } = require('./helpers')

test('update (add a feedId)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const bob = Server()
  bob.wiki = new Crut(bob, Spec())

  const init = { body: 'dinosaur notes', authors: { add: [alice.id] } }
  alice.wiki.create(init, (err, id) => {
    t.error(err, 'alice creates a record with self as author')

    const A = { body: 'epistomology notes' }
    alice.wiki.update(id, A, (err, updateA) => {
      t.error(err, 'alice can publish an update')

      replicate({ from: alice, to: bob }, (err) => {
        if (err) throw err

        const B = { body: 'epistomology notes' }
        bob.wiki.update(id, B, (err) => {
          t.match(err.message, /Invalid author/, 'bob is not allowed to update')

          const C = { authors: { add: [bob.id] } }
          alice.wiki.update(id, C, (err) => {
            t.error(err, 'alice adds bob as an author')

            replicate({ from: alice, to: bob }, (err) => {
              if (err) throw err

              const D = { body: '<3' }
              bob.wiki.update(id, D, (err, updateId) => {
                t.error(err, 'bob can now publish updates')

                /* testing #get */
                const expected = {
                  key: id,
                  type: 'wiki',
                  originalAuthor: alice.id,
                  recps: null,
                  states: [{
                    key: updateId,
                    authors: {
                      [alice.id]: [{ start: 0, end: null }],
                      [bob.id]: [{ start: 0, end: null }]
                    },
                    body: '<3',
                    tombstone: null
                  }]
                }

                compareRead(alice, bob, id, expected, t, (err) => {
                  if (err) throw err

                  alice.close()
                  bob.close()
                  t.end()
                })
              })
            })
          })
        })
      })
    })
  })
})

test('update (add *)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const bob = Server()
  bob.wiki = new Crut(bob, Spec())

  const init = { body: 'dinosaur notes', authors: { add: [alice.id] } }
  alice.wiki.create(init, (err, id) => {
    t.error(err, 'alice creates a record with self as author')

    const A = { body: 'epistomology notes' }
    alice.wiki.update(id, A, (err, updateA) => {
      t.error(err, 'alice can publish an update')

      replicate({ from: alice, to: bob }, (err) => {
        if (err) throw err

        const B = { body: 'epistomology notes' }
        bob.wiki.update(id, B, (err) => {
          t.match(err.message, /Invalid author/, 'bob is not allowed to update')

          const C = { authors: { add: ['*'] } }
          alice.wiki.update(id, C, (err, updateId) => {
            alice.get(updateId, (_, update) => {
              const timestamp = Number(Object.keys(update.content.authors['*'])[0])

              t.error(err, 'alice adds * as an author')

              replicate({ from: alice, to: bob }, (err) => {
                if (err) throw err

                const D = { body: '<3' }
                bob.wiki.update(id, D, (err, updateId) => {
                  t.error(err, 'bob can now publish updates')

                  /* testing #get */
                  const expected = {
                    key: id,
                    type: 'wiki',
                    originalAuthor: alice.id,
                    recps: null,
                    states: [{
                      key: updateId,
                      authors: {
                        [alice.id]: [{ start: 0, end: null }],
                        '*': [{ start: timestamp, end: null }]
                      },
                      tombstone: null,
                      body: '<3'
                    }]
                  }

                  compareRead(alice, bob, id, expected, t, (err) => {
                    if (err) throw err

                    alice.close()
                    bob.close()
                    t.end()
                  })
                })
              })
            })
          })
        })
      })
    })
  })
})

test('update (remove feedId, add it back)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const bob = Server()
  bob.wiki = new Crut(bob, Spec())

  const init = { body: 'dinosaur notes', authors: { add: [alice.id], remove: [bob.id] } }
  alice.wiki.create(init, (err, id) => {
    t.error(err, 'alice adds self as author, removes bob')

    replicate({ from: alice, to: bob }, (err) => {
      if (err) throw err

      const B = { body: 'epistomology notes' }
      bob.wiki.update(id, B, (err) => {
        t.match(err.message, /Invalid author/, 'bob is not allowed to update')

        const C = { authors: { add: [bob.id] } }
        alice.wiki.update(id, C, (err) => {
          t.error(err, 'alice adds bob as an author')

          replicate({ from: alice, to: bob }, (err) => {
            if (err) throw err

            const D = { body: '<3' }
            bob.wiki.update(id, D, (err, updateId) => {
              t.error(err, 'bob can now publish updates')

              /* testing #get */
              const expected = {
                key: id,
                type: 'wiki',
                originalAuthor: alice.id,
                recps: null,
                states: [{
                  key: updateId,
                  authors: {
                    [alice.id]: [{ start: 0, end: null }],
                    [bob.id]: [{ start: 0, end: null }]
                  },
                  tombstone: null,
                  body: '<3'
                }]
              }
              compareRead(alice, bob, id, expected, t, (err) => {
                if (err) throw err

                alice.close()
                bob.close()
                t.end()
              })
            })
          })
        })
      })
    })
  })
})

test('update (remove feedId, add *)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const bob = Server()
  bob.wiki = new Crut(bob, Spec())

  const init = { body: 'dinosaur notes', authors: { add: [alice.id], remove: [bob.id] } }
  alice.wiki.create(init, (err, id) => {
    t.error(err, 'alice adds self as author, removes bob')

    replicate({ from: alice, to: bob }, (err) => {
      if (err) throw err

      const B = { body: 'epistomology notes' }
      bob.wiki.update(id, B, (err) => {
        t.match(err.message, /Invalid author/, 'bob is not allowed to update')

        const C = { authors: { add: ['*'] } }
        alice.wiki.update(id, C, (err, updateId) => {
          t.error(err, 'alice adds * as an author')

          alice.get(updateId, (_, update) => {
            const timestamp = Number(Object.keys(update.content.authors['*'])[0])

            replicate({ from: alice, to: bob }, (err) => {
              if (err) throw err

              const D = { body: '<3' }
              bob.wiki.update(id, D, (err, updateId) => {
                t.error(err, 'bob can now publish updates')

                /* testing #get */
                const expected = {
                  key: id,
                  type: 'wiki',
                  originalAuthor: alice.id,
                  recps: null,
                  states: [{
                    key: updateId,
                    authors: {
                      [alice.id]: [{ start: 0, end: null }],
                      '*': [{ start: timestamp, end: null }]
                    },
                    tombstone: null,
                    body: '<3'
                  }]
                }
                compareRead(alice, bob, id, expected, t, (err) => {
                  if (err) throw err

                  alice.close()
                  bob.close()
                  t.end()
                })
              })
            })
          })
        })
      })
    })
  })
})

test('update (add a feedId, later remove it after it has published some)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const bob = Server()
  bob.wiki = new Crut(bob, Spec())

  const init = { body: 'dinosaur notes', authors: { add: [alice.id, bob.id] } }
  alice.wiki.create(init, (err, id) => {
    if (err) throw new Error(err)
    t.error(err, 'alice adds self + bob as authors')

    replicate({ from: alice, to: bob }, (err) => {
      if (err) throw err

      const A = { body: '<3' }
      bob.wiki.update(id, A, (err) => {
        t.error(err, 'bob can update')

        replicate({ from: bob, to: alice }, (err) => {
          if (err) throw err

          const B = { authors: { remove: [bob.id] } }
          alice.wiki.update(id, B, (err, updateId) => {
            t.error(err, 'alice removes bob')

            replicate({ from: alice, to: bob }, (err) => {
              if (err) throw err

              const C = { body: '):' }
              bob.wiki.update(id, C, (err) => {
                t.match(err.message, /Invalid author/, 'bob can no longer edit')

                /* testing #get */
                const expected = {
                  key: id,
                  type: 'wiki',
                  originalAuthor: alice.id,
                  recps: null,
                  states: [{
                    key: updateId,
                    authors: {
                      [alice.id]: [{ start: 0, end: null }],
                      [bob.id]: [{ start: 0, end: 1 }]
                    },
                    body: '<3',
                    tombstone: null
                  }]
                }
                compareRead(alice, bob, id, expected, t, (err) => {
                  if (err) throw err

                  alice.close()
                  bob.close()
                  t.end()
                })
              })
            })
          })
        })
      })
    })
  })
})

/* unhappy cases */
test('update (cannot remove last author)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const init = { body: 'dinosaur notes', authors: { add: [alice.id] } }
  alice.wiki.create(init, (err, id) => {
    t.error(err, 'alice creates a record with self as author')

    const A = { authors: { remove: [alice.id] } }
    alice.wiki.update(id, A, (err) => {
      t.match(err.message, /Invalid authors change/, 'alice cannot remove herself')

      alice.close()
      t.end()
    })
  })
})

test('update (cannot remove last author)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const bob = Server()

  const init = { body: 'dinosaur notes', authors: { add: [alice.id, bob.id] } }
  alice.wiki.create(init, (err, id) => {
    t.error(err, 'alice creates a record with self + bob as authors')

    const A = { authors: { remove: [alice.id] } }
    alice.wiki.update(id, A, (err) => {
      t.error(err, 'alice can remove self')

      alice.close()
      bob.close()
      t.end()
    })
  })
})
