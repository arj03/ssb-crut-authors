const Server = require('scuttle-testbot')
module.exports = function (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   keys: SecretKeys
  // }

  var stack = Server // eslint-disable-line
    // .use(require('ssb-private1'))
    .use(require('ssb-backlinks'))

  // if (opts.tribes || opts.recpsGuard) {
  //   stack = stack
  //     .use(require('ssb-query'))
  //     .use(require('ssb-tribes'))
  //   // only add ssb-tribes when testing recps, as it keystore startup takes 500ms
  // }

  // if (opts.recpsGuard === true) {
  //   stack = stack.use(require('ssb-recps-guard'))
  // }

  return stack(opts)
}
