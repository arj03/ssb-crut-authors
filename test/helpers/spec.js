const Overwrite = require('@tangle/overwrite')

module.exports = function Spec (opts = {}) {
  return Object.assign(
    {
      type: 'wiki',
      props: {
        body: Overwrite()
      }
    },
    opts
  )
}
