const { replicate } = require('scuttle-testbot')

module.exports = function compareRead (peerA, peerB, id, expected, t, cb) {
  // make sure A and B are both in sync with each others feeds
  //
  replicate({ from: peerA, to: peerB }, (err) => {
    if (err) return cb(err)

    replicate({ from: peerB, to: peerA }, (err) => {
      if (err) return cb(err)

      peerA.wiki.read(id, (err, recordA) => {
        if (err) return cb(err)

        peerB.wiki.read(id, (err, recordB) => {
          if (err) return cb(err)

          t.deepEqual(recordA, expected, `${peerA.id} has expected record state`)
          t.deepEqual(recordB, expected, `${peerB.id} has expected record state`)
          cb(null)
        })
      })
    })
  })
}
