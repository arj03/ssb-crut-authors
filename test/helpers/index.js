module.exports = {
  replicate: require('scuttle-testbot').replicate,
  compareRead: require('./compare-read'),
  Server: require('./server'),
  Spec: require('./spec')
}
