const test = require('tape')
const Crut = require('../')
const { Server, Spec } = require('./helpers')

const body = 'dinosaur notes'

function Expected (server, id, authors) {
  return {
    key: id,
    type: 'wiki',
    originalAuthor: server.id,
    recps: null,
    states: [{
      key: id,
      body: 'dinosaur notes',
      authors, // <<
      tombstone: null
    }]
  }
}

test('create (with authors.add FeedId)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const A = { body, authors: { add: [alice.id] } }
  alice.wiki.create(A, (err, id) => {
    t.error(err, 'creates record')
    alice.wiki.read(id, (_, record) => {
      const expected = Expected(alice, id, {
        [alice.id]: [{ start: 0, end: null }]
      })

      t.deepEqual(record, expected, 'can read')

      alice.close()
      t.end()
    })
  })
})

test('create (with authors.add *)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const A = { body, authors: { add: ['*'] } }
  alice.wiki.create(A, (err, id) => {
    t.error(err, 'creates record')
    alice.wiki.read(id, (_, record) => {
      const expected = Expected(alice, id, {
        '*': [{ start: record.states[0].authors['*'][0].start, end: null }]
      })

      t.deepEqual(record, expected, 'can read')

      alice.close()
      t.end()
    })
  })
})

/* unhappy cases */

test('create (no authors)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const A = { body }
  alice.wiki.create(A, (err, id) => {
    t.match(err.message, /Invalid initial authors/, '#create rejects no authors')
    alice.close()
    t.end()
  })
})

test('create (empty authors)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const A = { body, authors: {} }
  alice.wiki.create(A, (err, id) => {
    t.match(err.message, /complexSet must be an Object of shape .*/, '#create rejects empty authors')
    alice.close()
    t.end()
  })
})

test('create (empty authors.add)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const A = { body, authors: { add: [] } }
  alice.wiki.create(A, (err, id) => {
    t.match(err.message, /Invalid initial authors/, '#create rejects empty authors.add')
    alice.close()
    t.end()
  })
})

test('create (with authors.remove)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const D = { authors: { remove: [alice.id] } }
  alice.wiki.create(D, (err, rootId) => {
    t.match(err.message, /Invalid initial authors/, '#create rejects only authors.remove')
    alice.close()
    t.end()
  })
})

test('create (with malformed authors.add)', t => {
  const alice = Server()
  alice.wiki = new Crut(alice, Spec())

  const E = { authors: { add: ['@dog'] } }
  alice.wiki.create(E, (err, rootId) => {
    t.match(err.message, /malformed feedId/, '#create rejects only authors.remove')

    alice.close()
    t.end()
  })
})
