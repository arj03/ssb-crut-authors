const test = require('tape')
const Crut = require('../')
const { Server, Spec } = require('./helpers')

test('init', t => {
  const server = Server()
  const spec = Spec()

  t.doesNotThrow(
    () => {
      new Crut(server, spec) // eslint-disable-line
      new Crut(server, spec) // eslint-disable-line
    },
    null,
    'spec can be re-used without throwing'
  )
  // previously we were monkey-patching the spec with spec.props.authors
  // this caused later re-use to throw because props.authors is protected

  server.close()

  t.end()
})
